# Алгоритмы восстановления деревьев суперпозиций на основе PCSTFast

Эксперименты, что приводились в дипломе представлены в Code.ipynb, в CodeSymbRepr.ipynb также представлены функции порождения корректных суперпозиций и код для их прогнозирования.

Для восстановления матриц суперпозиции используется алгоритм PCSTFast, описанный в

> [A Fast, Adaptive Variant of the Goemans-Williamson Scheme for the Prize-Collecting Steiner Tree Problem](http://people.csail.mit.edu/ludwigs/papers/dimacs14_fastpcst.pdf)
> Chinmay Hegde, Piotr Indyk, Ludwig Schmidt
> Workshop of the 11th DIMACS Implementation Challenge: Steiner Tree Problems, 2014

реализация которого основана на

> [A Nearly-Linear Time Framework for Graph-Structured Sparsity](http://people.csail.mit.edu/ludwigs/papers/icml15_graphsparsity.pdf)
> Chinmay Hegde, Piotr Indyk, Ludwig Schmidt
> ICML 2015

Код этой реализации - см. проект [PCSTFast](https://github.com/fraenkel-lab/pcst_fast)

Большая часть современных алгоритмов (эвристик) для решения задачи PCST (Prize-Collecting Steiner Tree) основаны на 
> [A General Approximation Technique For Constrained Forest Problems](https://math.mit.edu/~goemans/PAPERS/GoemansWilliamson-1995-AGeneralApproximationTechniqueForConstrainedForestProblems.pdf)
>  Michel X. Goemans and David E. Williamson
> SIAM 1995


В Code.ipynb так же есть некоторые примеры использования этих алгоритмов.



